const mongoose = require("mongoose");
const roleUser = require("../constants/user");
const Channel = require("../models/channelModel");
const User = require("../models/userModel");
const userGrpc = require("../service/user-grpc-service");

module.exports.createChannel = async (req, res, next) => {
  try {
    // instructorId;
    // studentId;
    const { receiveId } = req.body;
    // console.log("Body: ", req.body);
    // userClientConn
    const userReceive = await userGrpc.getUserByIdGrpc(
      userClientConn,
      receiveId
    );
    console.log("userReceive: ", userReceive);

    if (!userReceive) {
      return res.status(404).json({ message: "User receive not existed" });
    }

    const userSender = await userGrpc.getUserByIdGrpc(
      userClientConn,
      req.user.sub
    );

    if (!userSender) {
      return res.status(404).json({ message: "User sender not existed" });
    }
    // console.log("receiveId: ", req.body.receiveId);
    let instructorId;
    let studentId;
    if (req.user.role == roleUser.NORMAL_USER) {
      if (userReceive.User.Role != roleUser.TEACHER) {
        return res.status(500).json({ message: "User receive must teacher" });
      }
      studentId = req.user.sub;
      instructorId = receiveId;
    } else {
      if (userReceive.User.Role != roleUser.NORMAL_USER) {
        return res.status(500).json({ message: "User receive must student" });
      }
      studentId = receiveId;
      instructorId = req.user.sub;
    }

    const findChannel = await Channel.findOne({
      instructorId,
      studentId,
    });

    if (findChannel) {
      return res.status(409).json({ message: "Channel already existed" });
    }

    const channel = await Channel.create({
      instructorId,
      studentId,
    });

    // User Receive
    const findUserReceive = await User.findOne({
      userId: userReceive.User.ID,
    }).exec();
    console.log("findUserReceive: ", findUserReceive);
    if (!findUserReceive) {
      await User.create({
        userId: userReceive.User.ID,
        name: userReceive.User.Name,
        role: userReceive.User.Role,
      });
    }

    // User Sender
    const findUserSender = await User.findOne({
      userId: userSender.User.ID,
    }).exec();

    if (!findUserSender) {
      console.log("create user");
      await User.create({
        userId: userSender.User.ID,
        name: userSender.User.Name,
        role: userSender.User.Role,
      });
    }

    return res.json({ data: channel, message: "Create channel successfully!" });
  } catch (ex) {
    // next(ex);
    return res.status(500).json({ message: ex.message });
  }
};

module.exports.getAllChannel = async (req, res, next) => {
  try {
 
    const channels = await Channel.aggregate([
      {
        $match: {
          $or: [{ instructorId: req.user.sub }, { studentId: req.user.sub }],
        },
      },
      {
        $lookup: {
          from: "users", // The name of the User collection in your database
          localField: "instructorId",
          foreignField: "userId",
          as: "instructor",
        },
      },
      {
        $lookup: {
          from: "users", // The name of the User collection in your database
          localField: "studentId",
          foreignField: "userId",
          as: "student",
        },
      },
      {
        $project: {
          _id: 1, // Include the channel's _id
          contact: {
            $cond: {
              if: { $eq: [req.user.role, roleUser.TEACHER] },
              then: { $arrayElemAt: ["$student", 0] },
              else: { $arrayElemAt: ["$instructor", 0] },
            },
          },
          createdAt: 1, // Include the channel's createdAt field
        },
      },
    ]);
   
    res.json({ data: channels });
  } catch (ex) {
    next(ex);
  }
};