const Message = require("../models/messageModel");
const Channel = require("../models/channelModel");
const { calculatePageSize } = require("../utils/common");
const mongoose = require("mongoose");

module.exports.addMessage = async (req, res, next) => {
  try {
    const { channelId, message } = req.body;
    // const data = await Messages.create({
    //   message: { text: message },
    //   users: [from, to],
    //   sender: from,
    // });

    const channel = await Channel.findById(
      new mongoose.Types.ObjectId(channelId)
    );

    if (!channel) {
      res.status(404).json({ message: "Channel not existed" });
      return;
    }
    const newMessage = await Message.create({
      channelId: channel._id, // Replace with the actual channel ID
      senderId: req.user.sub, // Replace with the actual sender's ID
      message: {
        text: message,
      },
    });

    return res.json({ msg: "Message added successfully." });
  } catch (ex) {
    // next(ex);
    console.log("ex: ", ex);
    return res
      .status(500)
      .json({ msg: "Failed to add message to the database" });
  }
};

module.exports.getMessagesByChannelId = async (req, res, next) => {
  const { channel_id, page, size } = req.query;

  const channel = await Channel.findById(
    new mongoose.Types.ObjectId(channel_id)
  );

  if (!channel) {
    res.status(404).json({ message: "Channel not existed" });
    return;
  }
  console.log("Channel: ", channel);
  console.log("Channel: ", req.user.sub);
  if (
    channel.studentId != req.user.sub &&
    channel.instructorId != req.user.sub
  ) {
    return res.status(403).json({ message: "Permission denied" });
  }
  const { skip, pageSize } = calculatePageSize(page, size);

  let totalMessages = await Message.find({
    channelId: channel_id, 
  }).count();

  let messages = await Message.find({
    channelId: channel_id, // Replace with the actual channel ID
  })
    .sort({ create_at: -1 })
    .skip(skip)
    .limit(pageSize);
    
  // const countData =  messages.length

  messages = messages;
  messages = messages.reverse();

  console.log("Count Data: ", totalMessages);
  console.log("Number page: ", Math.ceil(totalMessages / pageSize));

  const totalPage = Math.ceil(totalMessages / pageSize)
  return res.json({
    msg: "Get messages successfully.",
    page: {
      size: +pageSize,
      page: +page,
      totalPage: totalPage,
      total: totalMessages,
    },
    data: messages,
  });
};
