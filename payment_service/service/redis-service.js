const { createClient } = require("redis");

module.exports.connectRedis = async () => {
  const clientRedis = createClient();

  clientRedis.on("error", (err) => console.log("Redis Client Error", err));
  await clientRedis.connect();
  console.log("Connect redis successfully!")
  global.clientRedis = clientRedis;
};
