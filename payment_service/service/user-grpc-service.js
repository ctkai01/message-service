const grpc = require("@grpc/grpc-js");
var protoLoader = require("@grpc/proto-loader");
var protoLoader = require("@grpc/proto-loader");
const fs = require("fs");
const path = require("path");
const PROTO_PATH = "grpc/users.proto";

const options = {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
};

var packageDefinition = protoLoader.loadSync(PROTO_PATH, options);

// const UsersService = grpc.loadPackageDefinition(packageDefinition).UsersService;
const protoDescriptor = grpc.loadPackageDefinition(packageDefinition);
const usersService = protoDescriptor.users_service;

const pem_root_certs = null; // You don't need to provide this
const private_key = fs.readFileSync("tls.key");
const cert_chain = fs.readFileSync("tls.crt");

const sslCreds = grpc.credentials.createSsl(
  pem_root_certs,
  private_key,
  cert_chain
);

module.exports.userClientConn = () => {
  const client = new usersService.UsersService(
    // "localhost:6004",
    // "https://test.kaidemy.click/",
    "localhost:42555",
    grpc.credentials.createInsecure()
    // sslCreds
  );
  global.userClientConn = client;
  console.log("Connect User GRPC successfully!")
};

module.exports.getUserByIdGrpc = (client, idUser) => {
  return new Promise((resolve, reject) => {
    client.GetUserByID(
      {
        UserId: idUser,
      },
      (error, user) => {
        if (!error) {
          console.log("User fetch: ", user);
          console.log("Error: ", error);
          resolve(user);
        } else {
          reject(new Error(`User with ID: ${idUser} not found`));
        }
      }
    );
  });
};
