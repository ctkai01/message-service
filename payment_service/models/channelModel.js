const mongoose = require("mongoose");

const channelSchema = new mongoose.Schema({
  instructorId: {
    type: Number,
    required: true,
    ref: "Users",
  },
  studentId: {
    type: Number,
    required: true,
    ref: "Users",
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Channels", channelSchema);
