const mongoose = require("mongoose");

const messageSchema = new mongoose.Schema({
  channelId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Channels", // Reference to the Channels collection
  },
  senderId: {
    type: Number, // Assuming this represents the sender's ID
    required: true,
  },
  message: {
    text: { type: String, required: true },
  },
  create_at: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Messages", messageSchema);
