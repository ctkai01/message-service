const passport = require("passport");
const roleUser = require("../constants/user");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: "secret-key", // Replace with your secret key
};

passport.use(
  new JwtStrategy(jwtOptions, (jwtPayload, done) => {
    // Check if the user has the required roles or permissions from jwtPayload

    if (jwtPayload.role == roleUser.NORMAL_USER || jwtPayload.role == roleUser.TEACHER) {
      return done(null, jwtPayload);
    } else {
      return done(null, false);
    }
  })
);

module.exports = passport;
