const {
  addMessage,
  getMessagesByChannelId,
} = require("../controllers/messageController");
const router = require("express").Router();

router.post("/", addMessage);
router.get("/", getMessagesByChannelId);
// GET /api/messages?channel_id=:channel_id
// router.get("/:id", getChannel);

module.exports = router;
